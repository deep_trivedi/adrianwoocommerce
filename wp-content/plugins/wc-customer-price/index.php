<?php
/*
Plugin Name: wc customer price
Description: WooCommerce customer Pricing - An extension for WooCommerce, which adds customer functionality to your store.
Version: 1.0.1
Author: E0101
Author URI: 
*/

add_action('admin_menu', 'woo_customer_page',99);
add_action('admin_init', 'register_woo_customer_settings');
add_option('co_savings_label', 'You Save', '', 'yes');
add_option('co_rrp_label', 'RRP', '', 'yes');
add_option('co_customer_label', 'Your Price', '', 'yes');
add_option('co_customer_role', 'regular_customer', '', 'yes');

function woo_customer_page() {
	add_submenu_page( 'woocommerce', 'Customer Pricing', 'Customer Pricing', 'manage_options', 'manage-customer-pricing', 'woo_customer_page_call' ); 
}

function register_woo_customer_settings() {
	register_setting( 'woo_customer_options', 'co_savings' );
	register_setting( 'woo_customer_options', 'co_savings_label' );
	register_setting( 'woo_customer_options', 'co_percentage' );
	register_setting( 'woo_customer_options', 'co_rrp' );
	register_setting( 'woo_customer_options', 'co_rrp_label' );
	register_setting( 'woo_customer_options', 'co_customer_label' );
	register_setting( 'woo_customer_options', 'co_min_quantity' );
	register_setting( 'woo_customer_options', 'co_min_quantity_value' );
	register_setting( 'woo_customer_options', 'co_max_quantity' );
	register_setting( 'woo_customer_options', 'co_max_quantity_value' );
	register_setting( 'woo_customer_options', 'co_customer_role' );
}

function woo_customer_page_call() {
	include('options-page.php');
} 

add_role('regular_customer', 'Regular Customer', array(
    'read' => true, 
    'edit_posts' => false,
    'delete_posts' => false, 
));

add_action( 'save_post', 'cp_save_simple_customer_price' );
function cp_save_simple_customer_price( $post_id ) {
	if (isset($_POST['_inline_edit']) && wp_verify_nonce($_POST['_inline_edit'], 'inlineeditnonce'))return;
	if (isset($_POST['customer_price'])){$new_data = $_POST['customer_price'];}
	if (isset($_POST['post_ID'])){$post_ID = $_POST['post_ID'];}
	update_post_meta($post_ID, '_customer_price', $new_data) ;
}

add_action( 'woocommerce_product_options_pricing', 'cp_add_admin_simple_customer_price', 10, 2 );
function cp_add_admin_simple_customer_price( $loop ){ 
	$customer = get_post_meta( get_the_ID(), '_customer_price', true );
	echo '<tr><td><div><p class="form-field _regular_price_field">';
	echo '<label>'.__( 'Customer Price', 'woocommerce' ) . ' ('.get_woocommerce_currency_symbol().')'.'</label>';
	echo '<input step="any" type="number" class="wc_input_price short" name="customer_price" value="'.$customer.'"/>';
	echo '</p></div></td></tr>';
}

add_action( 'woocommerce_product_after_variable_attributes', 'cp_add_variable_customer_price', 10, 3 );
add_action( 'woocommerce_product_after_variable_attributes_js', 'cp_add_variable_customer_price_js' );
add_action( 'woocommerce_process_product_meta_variable', 'cp_variable_customer_price_process', 10, 1 );
function cp_add_variable_customer_price( $loop, $variation_data, $variation ) {
	
	$get_variation_id = $variation->ID;
	$customer_price = get_post_meta($get_variation_id, '_customer_price', true );
	
	echo '<tr><td><div>';
	echo '<label>'.__( 'Customer Price', 'woocommerce' ) . ' ('.get_woocommerce_currency_symbol().')'.'</label>';
	echo '<input step="any" type="number" size="5" name="customer['.$loop.']" value="'.$customer_price.'"/>';
	echo '</div></td></tr>';
}

function cp_add_variable_customer_price_js() {
	echo '<tr><td><div>';
	echo '<label>'.__( 'Customer Price', 'woocommerce' ) . ' ('.get_woocommerce_currency_symbol().')'.'</label>';
	echo '<input step="any" type="number" size="5" name="customer[" + loop + "]" />';
	echo '</div></td></tr>';
}

function cp_variable_customer_price_process( $post_id ) {
if (isset( $_POST['variable_sku'] ) ) :
    $variable_sku = $_POST['variable_sku'];
    $variable_post_id = $_POST['variable_post_id'];
    $customer_field = $_POST['customer'];
    for ( $i = 0; $i < sizeof( $variable_sku ); $i++ ) :
        $variation_id = (int) $variable_post_id[$i];
        if ( isset( $customer_field[$i] ) ) {
            update_post_meta( $variation_id, '_customer_price', stripslashes( $customer_field[$i] ) );
			update_post_meta( $variation_id, '_parent_product', $post_id );
        }
    endfor;
	update_post_meta( $post_id, '_variation_prices', $customer_field );
	update_post_meta( $post_id, '_customer_price', '' );
endif;
}


add_filter( 'manage_edit-product_columns', 'cp_add_customer_column' ) ;
function cp_add_customer_column( $columns ) {
	$offset = 2;
	$newArray = array_slice($columns, 0, $offset, true) +
	array('customer' => 'customer') +	array_slice($columns, $offset, NULL, true);
	return $newArray;
}

add_action( 'manage_product_posts_custom_column', 'cp_manage_customer_product_columns', 10, 2 );
function cp_manage_customer_product_columns( $column, $post_id ) {
	global $post;
	switch( $column ) {
		case 'customer' :
			$customer = get_post_meta( get_the_ID(), '_customer_price', true );
			if ( empty( $customer ) )
				echo __( '--' );
			else
				echo woocommerce_price($customer);
			break;
	}
}

add_filter( 'manage_edit-product_sortable_columns', 'cp_sortable_customer_column' );
function cp_sortable_customer_column( $columns ) {
	$columns['customer'] = 'customer';
	return $columns;
}

add_filter('woocommerce_variable_price_html', 'cp_custom_variation_price', 10, 2);
function cp_custom_variation_price( $price, $product ) {
	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('co_customer_role');
	foreach ($user_roles as $roles) {
		if ($roles == $current_role ){
			$variations = $product->get_available_variations();
			$lowestvar = array();
			foreach ($variations as $variation){
				$lowestvar[] = get_post_meta($variation['variation_id'],'_customer_price', true);
				array_multisort($lowestvar, SORT_ASC);
			}
			$minp = min($lowestvar);
			$maxp = max($lowestvar);
			if ($minp == $maxp){$price = woocommerce_price($minp);} else {$price = woocommerce_price($minp).' - '.woocommerce_price($maxp); }
			}
		}
	return $price;
}

add_filter( 'woocommerce_available_variation', 'cp_update_dropdown_variation_price', 10, 3);
function cp_update_dropdown_variation_price( $data, $product, $variation ) {
	$data['price_html'] = '<span class="price">'.woocommerce_price(get_post_meta( $data['variation_id'], '_price', true )).'</span>';
	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('co_customer_role');
	$customerp = get_post_meta( $data['variation_id'], '_customer_price', true );
	foreach ($user_roles as $roles) {
		if  ($roles == $current_role ){
			if ($customerp !== ''){
   				$data['price_html'] = '<span class="price">'.woocommerce_price($customerp).'</span>'; 
			}
		}  
	} 
	return $data;
}

add_action( 'woocommerce_get_price_html' , 'cp_get_customer_price' );
function cp_get_customer_price($price){

	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('co_customer_role');
	foreach ($user_roles as $roles) {
	
		if  ($roles == $current_role ){
		
			$customer = get_post_meta( get_the_ID(), '_customer_price', true );
			$rrp = get_post_meta( get_the_ID(), '_price', true );
			$savings  = $rrp - $customer;
			$division = $rrp ? $savings / $rrp : 0;
			$co_percentage = get_option( 'co_percentage' );
			$co_savings = get_option( 'co_savings' );
			$co_rrp = get_option( 'co_rrp' );
			$res = $division * 100;
			$res = round($res, 0);
			$res = round($res, 1);
			$res = round($res, 2);
			if ($customer){

				if ($co_rrp == '1' && $co_percentage == '1' && $co_savings == '1' ) {
					$price = get_option( 'co_rrp_label' ).': '.woocommerce_price($rrp).'</br>'.get_option( 'co_customer_label' ).': '.woocommerce_price($customer).'</br>'.get_option( 'co_savings_label' ).': '.woocommerce_price($savings).' ('.$res.'%)';	
				} 

				elseif ($co_rrp == '' && $co_percentage == '1' && $co_savings == '1' ) {
					$price = get_option( 'co_customer_label' ).': '.woocommerce_price($customer).'</br>'.get_option( 'co_savings_label' ).': '.woocommerce_price($savings).' ('.$res.'%)';
				}

				elseif ($co_rrp == '' && $co_percentage == '' && $co_savings == '1' ) {
					$price = get_option( 'co_customer_label' ).': '.woocommerce_price($customer).'</br>'.get_option( 'co_savings_label' ).': '.woocommerce_price($savings);
				}

				elseif ($co_rrp == '' && $co_percentage == '1' && $co_savings == '' ) {
					$price = get_option( 'co_customer_label' ).': '.woocommerce_price($customer).'</br>'.get_option( 'co_savings_label' ).': ('.$res.'%)';
				}

				elseif ($co_rrp == '1' && $co_percentage == '' && $co_savings == '' ) {
					$price = get_option( 'co_customer_label' ).': '.woocommerce_price($customer).'</br>'.get_option( 'co_rrp_label' ).': '.woocommerce_price($rrp);
				}

				elseif ($co_rrp == '1' && $co_percentage == '1' && $co_savings == '' ) {
					$price = get_option( 'co_customer_label' ).': '.woocommerce_price($customer).'</br>'.get_option( 'co_rrp_label' ).': '.woocommerce_price($rrp).'</br>'.get_option( 'co_savings_label' ).': ('.$res.'%)';
				}

				elseif ($co_rrp == '1' && $co_percentage == '' && $co_savings == '1' ) {
					$price = get_option( 'co_customer_label' ).': '.woocommerce_price($customer).'</br>'.get_option( 'co_rrp_label' ).': '.woocommerce_price($rrp).'</br>'.get_option( 'co_savings_label' ).': '.woocommerce_price($savings);
				}

				elseif ($co_rrp == '' && $co_percentage == '' && $co_savings == '' ) {
					$price = woocommerce_price($customer);
				}		
			}
		}
	}
return $price;	
}

add_action( 'woocommerce_before_calculate_totals', 'cp_simple_add_cart_price' );
function cp_simple_add_cart_price( $cart_object ) {
	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('co_customer_role');
	foreach ($user_roles as $roles) {
		if  ($roles == $current_role ){
 			foreach ( $cart_object->cart_contents as $key => $value ) {
				$customer = get_post_meta( $value['data']->id, '_customer_price', true );
				$customerv = get_post_meta( $value['data']->variation_id, '_customer_price', true );

				if ($customer){
					$value['data']->price = $customer;
				}
				if ($customerv){
					$value['data']->price = $customerv;
				}
			} 
		}
	}
}

add_filter( 'woocommerce_quantity_input_min', 'cp_add_minimum_quantity' );

function cp_add_minimum_quantity($input_value) {

	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('co_customer_role');
	foreach ($user_roles as $roles) {
		if ($roles == $current_role ){
			if (get_option( 'co_min_quantity' ) == '1' ) {
				return get_option( 'co_min_quantity_value' );			
			} else {
				return $input_value;
			}		
		}
	}
}

add_filter( 'woocommerce_quantity_input_max', 'cp_add_maximum_quantity' );
function cp_add_maximum_quantity($input_value) {
	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('co_customer_role');
	foreach ($user_roles as $roles) {
		if  ($roles == $current_role ){
			if (get_option( 'co_max_quantity' ) == '1' ) {
				return get_option( 'co_max_quantity_value' );			
			} else {
				return $input_value;
			}		
		}
	}
}

function cp_mini_cart_prices( $product_price, $values, $cart_item) {
	
	global $woocommerce;
	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('co_customer_role');
	$varwp = get_post_meta( $values['variation_id'], '_customer_price', true );
	$varnp = get_post_meta( $values['variation_id'], '_price', true );
	$simplewp = get_post_meta( $values['product_id'], '_customer_price', true );
	$simplenp = get_post_meta( $values['product_id'], '_price', true );
	foreach ($user_roles as $roles) {
		if ($roles == $current_role ){
			if ($values['variation_id'] > 0 ){
				if ($varwp == ''){return woocommerce_price($varnp);} else {return woocommerce_price($varwp);}
			} else {
				if ($simplewp == ''){return woocommerce_price($simplenp);} else {return woocommerce_price($simplewp);}
			}
		}
	}
	
return $product_price;
	
}
add_filter('woocommerce_cart_item_price', 'cp_mini_cart_prices', 10, 3);



////////// NEEDS WORK //////////



//add_action( 'pre_get_posts', 'cp_only_customer_products' );
 
function cp_only_customer_products( $q ) {
	
	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('co_customer_role');
	
	if ( ! $q->is_main_query() ) return;
	if ( ! $q->is_post_type_archive() ) return;
	
	if ( ! is_admin() && is_shop() ) {
		
		foreach ($user_roles as $roles) {
			if  ($roles == $current_role ){
				$q->set( 'meta_query', array(array(
					'key' => '_customer_price',
					'value' => '',
					'compare' => '!='
				)));
			}
		}
	
	}
	remove_action( 'pre_get_posts', 'cp_only_customer_products' );
}