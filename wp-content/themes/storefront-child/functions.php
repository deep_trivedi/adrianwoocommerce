<?php
//echo current_time( 'timestamp', 1 );
//echo "current_time( 'mysql' ) returns local site time: " . current_time( 'mysql' ) . '<br />';
//echo "current_time( 'mysql', 1 ) returns GMT: " . current_time( 'mysql', 1 ) . '<br />';
//echo "current_time( 'timestamp' ) returns local site time: " . date( 'Y-m-d H:i:s', current_time( 'timestamp', 0 ) );
//echo "current_time( 'timestamp', 1 ) returns GMT: " . date( 'Y-m-d H:i:s', current_time( 'timestamp', 1 ) );
ini_set( 'max_execution_time', 10000 );
ini_set( 'memory_limit', '1024M' );
error_reporting( E_ALL ^ E_NOTICE );

add_action( 'wp_enqueue_scripts', 'sf_enqueue_styles' );
function sf_enqueue_styles() {
    wp_enqueue_style( 'sf-style', get_template_directory_uri() . '/style.css' );

}

// create a scheduled event (if it does not exist already)
function sf_cron_job_set() {
 //echo $timestamp= wp_next_scheduled( 'sf_woo_product_import_csv' ).'ssss';
//wp_clear_scheduled_hook('sf_woo_product_import_csv');
//wp_unschedule_event( $timestamp, 'sf_woo_product_import_csv' );
	//if( !wp_next_scheduled( 'sf_woo_product_import_csv' ) ) {  
	//   wp_schedule_event( current_time( 'timestamp') , 'hourly', 'sf_woo_product_import_csv' );  
	//}
}
//add_action( 'wp', 'sf_cron_job_set' );



// add custom interval
function sf_cron_add_minute( $schedules ) {
	// Adds once every minute to the existing schedules.
    $schedules['everyminute'] = array(
	    'interval' => 60,
	    'display' => __( 'Once Every Minute' )
    );
    return $schedules;
}
//add_filter( 'cron_schedules', 'sf_cron_add_minute' );



add_action( 'sf_woo_product_import_csv', 'sf_woo_product_import_call' );
function sf_woo_product_import_call() {
	$dir =  $_SERVER['DOCUMENT_ROOT'].'/product_import'; // The directory containing the files. 
	$ext = '.csv'; 
	$files = glob( $dir . '/*' . $ext );
	foreach ( $files as $csvFile ) {
		$csv = readCSV( $csvFile );
	}				
}


//CODE|DSC|REMARKS|COL1 |QTY IN UNITS|SELL_4/PACKSIZE|SELL_5/PACKSIZE|GRP DSC|CATEGORY DSC|
//Code|Dsc|Remarks|Col1|QtyInUnits|Sell_4/Packsize|Sell_5/Packsize|GrpDsc|CategoryDsci|MinOrd|ConsumPr

//SKU|Product Name|Product Short Desc.|Visibility|Stock(Qty)|Regular price|Employee Price|Category|Tag|Minimum Quantity|Customer Price
//SKU|Product Name|Product Short Desc.|Visibility|Stock(Qty)|Customer Price|Employee Price|Category|Tag|Minimum Quantity|Regular price

function readCSV( $csvFile ){
$row = 1;
	$file_handle = fopen( $csvFile, 'r' );
	while ( !feof( $file_handle ) ) {
		$data= fgetcsv( $file_handle, 1024 );
		$all_data= $data[0];
	
		if( $all_data && $row != 1){
			$explode_data=explode( '|',$all_data );
			
			//if( $explode_data[0]!='CODE' ){
				 $sku = trim($explode_data[0]);
				 $post_title = trim($explode_data[1]);
				 $short_desc = trim($explode_data[2]);
				 $visibilty = trim($explode_data[3]);				
				 $stock = trim($explode_data[4]);
				 $customer_price = trim($explode_data[5]);
				 $employee_price = trim($explode_data[6]);
				 $category = trim($explode_data[7]);
				 $tag = trim($explode_data[8]);
				 $min_qty = trim($explode_data[9]);
				 $regular_price = trim($explode_data[10]);
			     $product_id_exists = product_exist_new( $sku );  
				
				$product_data = array(
						'post_type' 	 => 'product',
						'menu_order' 	 => '',
						'post_status' 	 => 'publish',
						'post_title'	 => $post_title,
						'post_content'	 => '',
						'post_excerpt'	 => $short_desc,
						'post_parent'	 => 0,
						'comment_status' => 'open',
						'menu_order'	 => 0,
						'post_author'	 => '',
						'ping_status'	 =>'open',
					);
					
				$product_meta_data = array(
						'_downloadable'  => 'no',
						'_virtual'   	 => 'no',
						'_manage_stock'  => 'yes',
						'_featured'		 => 'no'
				);	
				//print_r($explode_data);
				
				// Check product already exists or not 
				if( $product_id_exists != '' && !empty($product_id_exists)){
						$product_data['ID'] = $product_id_exists;
						
						$exist_sku = get_post_meta( $product_id_exists, '_sku', true ); 
						 
						if($exist_sku == $sku) {
							//echo "Update".$product_id_exists.":::::".$exist_sku."::::::::::::".$sku.'<br/>';
						    wp_update_post( $product_data );
							 
							//update_post_meta( $product_id_exists, '_sku', $sku );
							update_post_meta( $product_id_exists, '_regular_price', $regular_price );
							update_post_meta( $product_id_exists, '_stock', $stock );	
							update_post_meta( $product_id_exists, '_price', $regular_price );
							update_post_meta( $product_id_exists, 'minimum_allowed_quantity', $min_qty );
							if($employee_price == 0){
								$employee_price = '0.00';
								update_post_meta( $product_id_exists, '_wholesale_price', $employee_price );	
							}else{
								update_post_meta( $product_id_exists, '_wholesale_price', $employee_price );	
							}
							if($customer_price == 0){
								$customer_price = '0.00';
								update_post_meta( $product_id_exists, '_customer_price', $customer_price );	
							}else{
								update_post_meta( $product_id_exists, '_customer_price', $customer_price );	
							}
							if( $visibilty == 'N' ){
								update_post_meta( $product_id_exists, '_visibility', 'hidden' );
							}else{
								update_post_meta( $product_id_exists, '_visibility', 'visible' );
							}
							if( $stock == '0' ){
								update_post_meta( $product_id_exists, '_stock_status', 'outofstock' );
								update_post_meta( $product_id_exists, '_backorders', 'no' );
							}else{
								update_post_meta( $product_id_exists, '_stock_status', 'instock' );
								update_post_meta( $product_id_exists, '_backorders', 'yes' );
							}
							
							//wp_set_object_terms( $product_id_exists, 'simple', 'product_type' );
							wp_set_object_terms( $product_id_exists, $category, 'product_cat' );
							wp_set_object_terms( $product_id_exists, $tag, 'product_tag' );
							
							$imageID = saveFeaturedImage($sku,$product_id_exists);
							if ($imageID){
								set_post_thumbnail( $product_id_exists, $imageID );	
							}
						
						}
						
				}else{
					    
				        $product_id = wp_insert_post( $product_data );
						if(!empty($product_id) && !empty($sku)){
							
							//echo "Insert".$product_id.":::".$sku."<br/>";
							update_post_meta( $product_id, '_sku', $sku );
							update_post_meta( $product_id, '_regular_price', $regular_price );
							update_post_meta( $product_id, '_stock', $stock );	
							update_post_meta( $product_id, '_price', $regular_price );
							update_post_meta( $product_id, 'minimum_allowed_quantity', $min_qty );							
							if($employee_price == 0){
								$employee_price = '0.00';
								update_post_meta( $product_id, '_wholesale_price', $employee_price );	
							}else{
								update_post_meta( $product_id, '_wholesale_price', $employee_price );	
							}
							if($customer_price == 0){
								$customer_price = '0.00';
								update_post_meta( $product_id, '_customer_price', $customer_price );	
							}else{
								update_post_meta( $product_id, '_customer_price', $customer_price );	
							}
					
							if( $visibilty == 'N' ){
								update_post_meta( $product_id, '_visibility', 'hidden' );
							}else{
								update_post_meta( $product_id, '_visibility', 'visible' );
							}
							if( $stock == '0' ){
								update_post_meta( $product_id, '_stock_status', 'outofstock' );
								update_post_meta( $product_id, '_backorders', 'no' );
							}else{
								update_post_meta( $product_id, '_stock_status', 'instock' );
								update_post_meta( $product_id, '_backorders', 'yes' );
							}
					
							foreach( $product_meta_data as $p_k=>$p_v ){
								update_post_meta( $product_id, $p_k , $p_v );
							}
							wp_set_object_terms( $product_id, 'simple', 'product_type' );
							wp_set_object_terms( $product_id, $category, 'product_cat' );
							wp_set_object_terms( $product_id, $tag, 'product_tag' );
							
							
							$imageID = saveFeaturedImage($sku,$product_id);
							if ($imageID){
								set_post_thumbnail( $product_id, $imageID );	
							}
					}
				}	
				
			//}
		}
		
	$row++;
	}
	fclose($file_handle);
	return $data;
}


function product_exist( $sku ) {
   global $wpdb;
   $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id,meta_value FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value= %s LIMIT 1", $sku ) );
   
   return $product_id;
}


function saveFeaturedImage($sku,$product_id) {
	
	$imageID = false;
	
	$attach_id = false;
	$upload_dir = wp_upload_dir();
	
	$image_path_dir = $_SERVER['DOCUMENT_ROOT'].'/images/';
	$image_path_url = site_url().'/images/';
	
	$filename_url = $image_path_url.$sku.'.jpg';
	$filename_dir = $image_path_dir.$sku.'.jpg';
	
	$imagename= $sku.'.jpg';
	
	if (file_exists($filename_dir)) {
		
		/* use curl to get image instead of $image_data = file_get_contents($image);*/
		
		$ch = curl_init();
		$timeout = 120;
		// curl set options
		curl_setopt ($ch, CURLOPT_URL, $filename_url);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt ($ch, CURLOPT_AUTOREFERER, true);
		
		// Getting binary data
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		
		//exec curl command
		$image_data = curl_exec($ch);
		
		
		/* !2.1.0 get the mime type incase there is no extension */
		$mime_type =  curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

	
		//close the curl command
		curl_close($ch);
		//$image_data = file_get_contents($filename_url);
		//get the filename
		/* ! development add sanatize filename if name has spaces or %20 */
		$filename =  sanitize_file_name( basename(urldecode($filename_url)) );
		
		if (wp_mkdir_p($upload_dir['path'])) {
			$file = $upload_dir['path'] . '/' . $filename;
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}
		if(!file_exists($file)){
			//split it up
			list($directory, , $extension, $filename) = array_values(pathinfo($file));
			
			
			$wp_filetype = wp_check_filetype($imagename, null );
			
			if (file_put_contents($file, $image_data)) {
				
				$wp_filetype = wp_check_filetype($filename, null );
				
				/* ! 2.1.0 added mime type */
				if (!$wp_filetype['type'] && !empty($mime_type)) {
					$allowed_content_types = wp_get_mime_types();
					
					if (in_array($mime_type, $allowed_content_types)){
						$wp_filetype['type'] = $mime_type;
					}
				}
				
				$attachment = array(
					'post_mime_type' => $wp_filetype['type'],
					'post_title' => sanitize_file_name($filename),
					'post_content' => '',
					'post_status' => 'inherit'
				);

				$attach_id = wp_insert_attachment( $attachment, $file );
				require_once ABSPATH . 'wp-admin/includes/image.php';
				$attach_data = @wp_generate_attachment_metadata( $attach_id, $file );
				wp_update_attachment_metadata( $attach_id, $attach_data );	
				
			}
			 return $attach_id;	
		}else{
			return false;
		}
			
	}	
}
add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs) {

 unset($tabs['reviews']);

 return $tabs;
}	

/* Change the 'Only # left in stock' message on the WooCommerce product page to
 * simply show 'Limited Stock'.
 * Add to your theme's functions.php file
 */
function custom_stock_totals($availability_html, $availability_text, $product) {
	if (substr($availability_text,0, 4)=="Only") {
		$availability_text = "Limited Stock";
	} 
	$availability_html = '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability_text ) . '</p>';
	return 	$availability_html;
}
add_filter('woocommerce_stock_html', 'custom_stock_totals', 20, 3);


/* change layout of product tag cloud
  */

function custom_woocommerce_tag_cloud_widget() {
    $args = array(
    	'smallest' => 14, 
		'largest' => 14,
		'unit'    => 'px', 		
        'format' => 'list', 
        'separator' => "\n", 
        'orderby' => 'name',
        'taxonomy' => 'product_tag'
    );
    return $args;
}
add_filter( 'woocommerce_product_tag_cloud_widget_args', 'custom_woocommerce_tag_cloud_widget' );

/*
* User group change only administartor.
*
* Function Name: sf_custom_user_profile_fields.
*
* @created by {E0101} and {15-05-2015}
*
**/
function sf_custom_user_profile_fields( $user ) {
	

?>
	<h3><?php _e('User Group'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th><label for="sf_nectar_group"><?php _e('Nectar customer id'); ?></label></th>
			<td>
				    <input type="text" name="nectar" id="nectar" value="<?php echo esc_attr( get_user_meta($user->ID, 'nectar', true) ); ?>" /><br />
	             					
			</td>
		</tr>
		<tr>
			<th><label for="sf_sweetsource_group"><?php _e('Sweetsource customer id'); ?></label></th>
			<td>
				    <input type="text" name="sweetsource" id="sweetsource" value="<?php echo esc_attr( get_user_meta($user->ID, 'sweetsource', true) ); ?>" /><br />
	             					
			</td>
		</tr>
	</table>
<?php
	} 

/**
* Save User group change only administartor.
*
* Function Name: sf_save_custom_user_profile_fields.
*
@created by {E0101} and {15-05-2015}
*
**/
function sf_save_custom_user_profile_fields( $user_id ) {
	
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	
	update_user_meta( $user_id, 'nectar', $_POST['nectar'] );
	update_user_meta( $user_id, 'sweetsource', $_POST['sweetsource'] );
	//update_usermeta( $user_id, 'sf_user_group', $_POST['sf_user_group'] );
}

add_action( 'show_user_profile', 'sf_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'sf_custom_user_profile_fields' );
add_action( 'user_new_form', 'sf_custom_user_profile_fields' );

add_action( 'personal_options_update', 'sf_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'sf_save_custom_user_profile_fields' );
add_action('user_register', 'sf_save_custom_user_profile_fields');

/**
* When user register then change group.
*
* Function Name: add_user_status.
*
@created by {E0101} and {15-05-2015}
*
**/ 
add_filter( 'manage_users_columns',  'sf_add_nectar_column'  );
add_filter( 'manage_users_columns',  'sf_add_sweetsource_column'  );
add_filter( 'manage_users_custom_column',  'sf_nectar_column', 10, 3 );
add_filter( 'manage_users_custom_column',  'sf_sweetsource_column', 10, 3 );

function sf_add_nectar_column( $columns ) {
        $the_columns['sf_nectar_group'] = __( 'Nectar customer id' );

        $newcol = array_slice( $columns, 0, -1 );
        $newcol = array_merge( $newcol, $the_columns );
        $columns = array_merge( $newcol, array_slice( $columns, 1 ) );

        return $columns;
}
function sf_add_sweetsource_column( $columns ) {
        $the_columns['sf_sweetsource_group'] = __( 'Sweetsource customer id' );

        $newcol = array_slice( $columns, 0, -1 );
        $newcol = array_merge( $newcol, $the_columns );
        $columns = array_merge( $newcol, array_slice( $columns, 1 ) );

        return $columns;
}
function sf_nectar_column( $val, $column_name, $user_id ) {
    switch ( $column_name ) {
        case 'sf_nectar_group' :
            return get_user_meta($user_id, 'nectar', true);
            break;

        default:
    }

    return $val;
}
function sf_sweetsource_column( $val, $column_name, $user_id ) {
    switch ( $column_name ) {
        case 'sf_sweetsource_group' :
            return get_user_meta($user_id, 'sweetsource', true);
            break;

        default:
    }

    return $val;
}
// Adding customer group details in order email to admin.
add_action( 'woocommerce_email_customer_details', 'sf_add_user_group_to_order_email', 10, 2 );
function sf_add_user_group_to_order_email( $order, $is_admin_email ) {
	$user_id = $order->user_id;
	$nectar_id = 'nectar';
	$sweetsource_id = 'sweetsource';
	$single = true;
	$user_nectar_id = get_user_meta($user_id, 'nectar', true);
	$user_sweetsource_id = get_user_meta($user_id, 'sweetsource', true);
	if(!empty($user_nectar_id))	{	
		echo '<p><h4>Nectar id:</h4> ' . $user_nectar_id . ' </p>';
		}
	if(!empty($user_sweetsource_id))	{
		echo '<p><h4>Sweetsource id:</h4> ' . $user_sweetsource_id . ' </p>';
		}
}
//Adding logout link in navigation menu for loged in users.
add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );
function add_loginout_link( $items, $args ) {
    if (is_user_logged_in() && $args->theme_location == 'primary') {
        $items .= '<li><a href="'. wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) ) .'">Log Out</a></li>';
    }
    elseif (!is_user_logged_in() && $args->theme_location == 'primary') {
        $items .= '<li><a href="' . get_permalink( woocommerce_get_page_id( 'myaccount' ) ) . '">Log In</a></li>';
    }
    return $items;
}

// Display 24 products per page. 
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );

//adding new price field for customers who are not employees
//add_action( 'woocommerce_product_options_pricing', 'sf_customer_product_field' );
function sf_customer_product_field() {
    woocommerce_wp_text_input( array( 'id' => 'customer_price', 'class' => 'wc_input_price short', 'label' => __( 'Customer Price', 'woocommerce' ) . ' (' . get_woocommerce_currency_symbol() . ')' ) );
}
//add_action( 'save_post', 'sf_customer_price_save_product' );
function sf_customer_price_save_product( $product_id ) {
    // If this is a auto save do nothing, we only save when update button is clicked
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;
	if ( isset( $_POST['customer_price'] ) ) {
		if ( is_numeric( $_POST['customer_price'] ) )
			update_post_meta( $product_id, 'customer_price', $_POST['customer_price'] );
	} else delete_post_meta( $product_id, 'customer_price' );
}


//list customer on product page
//add_filter( 'woocommerce_available_variation', 'sf_update_dropdown_variation_price', 10, 3);
function sf_update_dropdown_variation_price( $data, $product, $variation ) {
	$data['price_html'] = '<span class="price">'.woocommerce_price(get_post_meta( $data['variation_id'], '_price', true )).'</span>';
	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('wwo_customer_role');
	$wholesalep = get_post_meta( $data['variation_id'], 'customer_price', true );
	foreach ($user_roles as $roles) {
		if  ($roles == $current_role ){
			if ($wholesalep !== ''){
   				$data['price_html'] = '<span class="price">'.woocommerce_price($wholesalep).'</span>'; 
			}
		}  
	} 
	return $data;
}

//add_action( 'woocommerce_get_price_html' , 'sf_get_wholesale_price' );
function sf_get_wholesale_price($price){

	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('wwo_customer_role');
	foreach ($user_roles as $roles) {
		if  ($roles == $current_role ){
			$wholesale = get_post_meta( get_the_ID(), '_wholesale_price', true );
			$rrp = get_post_meta( get_the_ID(), '_price', true );
			$savings  = $rrp - $wholesale;
			$division = $rrp ? $savings / $rrp : 0;
			$wwo_percentage = get_option( 'wwo_percentage' );
			$wwo_savings = get_option( 'wwo_savings' );
			$wwo_rrp = get_option( 'wwo_rrp' );
			$res = $division * 100;
			$res = round($res, 0);
			$res = round($res, 1);
			$res = round($res, 2);
			if ($wholesale){

				if ($wwo_rrp == '1' && $wwo_percentage == '1' && $wwo_savings == '1' ) {
					$price = get_option( 'wwo_rrp_label' ).': '.woocommerce_price($rrp).'</br>'.get_option( 'wwo_wholesale_label' ).': '.woocommerce_price($wholesale).'</br>'.get_option( 'wwo_savings_label' ).': '.woocommerce_price($savings).' ('.$res.'%)';	
				} 

				elseif ($wwo_rrp == '' && $wwo_percentage == '1' && $wwo_savings == '1' ) {
					$price = get_option( 'wwo_wholesale_label' ).': '.woocommerce_price($wholesale).'</br>'.get_option( 'wwo_savings_label' ).': '.woocommerce_price($savings).' ('.$res.'%)';
				}

				elseif ($wwo_rrp == '' && $wwo_percentage == '' && $wwo_savings == '1' ) {
					$price = get_option( 'wwo_wholesale_label' ).': '.woocommerce_price($wholesale).'</br>'.get_option( 'wwo_savings_label' ).': '.woocommerce_price($savings);
				}

				elseif ($wwo_rrp == '' && $wwo_percentage == '1' && $wwo_savings == '' ) {
					$price = get_option( 'wwo_wholesale_label' ).': '.woocommerce_price($wholesale).'</br>'.get_option( 'wwo_savings_label' ).': ('.$res.'%)';
				}

				elseif ($wwo_rrp == '1' && $wwo_percentage == '' && $wwo_savings == '' ) {
					$price = get_option( 'wwo_wholesale_label' ).': '.woocommerce_price($wholesale).'</br>'.get_option( 'wwo_rrp_label' ).': '.woocommerce_price($rrp);
				}

				elseif ($wwo_rrp == '1' && $wwo_percentage == '1' && $wwo_savings == '' ) {
					$price = get_option( 'wwo_wholesale_label' ).': '.woocommerce_price($wholesale).'</br>'.get_option( 'wwo_rrp_label' ).': '.woocommerce_price($rrp).'</br>'.get_option( 'wwo_savings_label' ).': ('.$res.'%)';
				}

				elseif ($wwo_rrp == '1' && $wwo_percentage == '' && $wwo_savings == '1' ) {
					$price = get_option( 'wwo_wholesale_label' ).': '.woocommerce_price($wholesale).'</br>'.get_option( 'wwo_rrp_label' ).': '.woocommerce_price($rrp).'</br>'.get_option( 'wwo_savings_label' ).': '.woocommerce_price($savings);
				}

				elseif ($wwo_rrp == '' && $wwo_percentage == '' && $wwo_savings == '' ) {
					$price = woocommerce_price($wholesale);
				}		
			}
		}
	}
return $price;	
}

//add_action( 'woocommerce_single_product_summary', 'sf_customer_price_show', 5 );
function sf_customer_price_show() {
    global $product;
	// Do not show this on variable products
	$current_user = new WP_User(wp_get_current_user()->ID);
	$user_roles = $current_user->roles;
	$current_role = get_option('wwo_wholesale_role');
	if($roles == $current_role){
	if ( $product->product_type <> 'variable' ) {
		$cp = get_post_meta( $product->id, 'customer_price', true );
		echo '<div class="woocommerce_msrp">';
		_e( 'customer_price: ', 'woocommerce' );
		echo '<span class="woocommerce-rrp-price">' . woocommerce_price( $cp ) . '</span>';
		echo '</div>';
	}
}
}
// Optional: To show on archive pages
//add_action( 'woocommerce_after_shop_loop_item_title', 'sf_customer_price_show' );

?>
