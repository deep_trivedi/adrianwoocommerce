<?php
include_once( $_SERVER['DOCUMENT_ROOT'].'/wp-load.php' );

ini_set( 'max_execution_time', 10000 );
ini_set( 'memory_limit', '1024M' );
error_reporting( E_ALL ^ E_NOTICE );

//CODE|DSC|REMARKS|COL1 |QTY IN UNITS|SELL_4/PACKSIZE|SELL_5/PACKSIZE|GRP DSC|CATEGORY DSC|

//SKU|Product Name|Product Short Desc.|Visibility|Stock(Qty)|Regular price|Employee Price|Category|Tag
function readCSVnew( $csvFile ){
	
	$row = 1;
	$file_handle = fopen( $csvFile, 'r' );
	while ( !feof( $file_handle ) ) {
		$data= fgetcsv( $file_handle, 1024 );
		$all_data= $data[0];
		if( $all_data && $row != 1){
			$explode_data=explode( '|',$all_data );
			//print_r($explode_data);
			//if( $explode_data[0]!='CODE' ){
				echo $sku = trim($explode_data[0]);
				echo ":";
				echo $post_title = $explode_data[1];
				echo ":";
				echo $short_desc = $explode_data[2];
				echo ":";
				echo $visibilty = trim($explode_data[3]);				
				echo ":";
				echo $stock = trim($explode_data[4]);
				echo ":";
				echo $regular_price = trim($explode_data[5]);
				echo ":";
				echo $employee_price = trim($explode_data[6]);
				echo ":";
				echo $category = trim($explode_data[7]);
				echo ":";
				echo $tag = trim($explode_data[8]);
				
			    echo $product_id_exists = product_exist_new( $sku ); 
				
				$product_data = array(
						'post_type' 	 => 'product',
						'menu_order' 	 => '',
						'post_status' 	 => 'publish',
						'post_title'	 => $post_title,
						'post_content'	 => '',
						'post_excerpt'	 => $short_desc,
						'post_parent'	 => 0,
						'comment_status' => 'open',
						'menu_order'	 => 0,
						'post_author'	 => '',
						'ping_status'	 =>'open',
					);
					
				$product_meta_data = array(
						'_downloadable'  => 'no',
						'_virtual'   	 => 'no',
						'_manage_stock'  => 'yes',
						'_featured'		 => 'no'
				);	
				// Check product already exists or not
				if( $product_id_exists !=''){
						$product_data['ID'] = $product_id_exists;
					    wp_update_post( $product_data );
						 
						update_post_meta( $product_id_exists, '_sku', $sku );
						update_post_meta( $product_id_exists, '_regular_price', $regular_price );
						update_post_meta( $product_id_exists, '_stock', $stock );	
						update_post_meta( $product_id_exists, '_price', $regular_price );
						update_post_meta( $product_id_exists, '_wholesale_price', $employee_price );	
						
						
						if( $visibilty == 'N' ){
							update_post_meta( $product_id_exists, '_visibility', 'hidden' );
						}else{
							update_post_meta( $product_id_exists, '_visibility', 'visible' );
						}
						if( $stock == '0' ){
							update_post_meta( $product_id_exists, '_stock_status', 'outofstock' );
							update_post_meta( $product_id_exists, '_backorders', 'no' );
						}else{
							update_post_meta( $product_id_exists, '_stock_status', 'instock' );
							update_post_meta( $product_id_exists, '_backorders', 'yes' );

						}
						
						wp_set_object_terms( $product_id_exists, 'simple', 'product_type' );
						wp_set_object_terms( $product_id_exists, $category, 'product_cat' );
						wp_set_object_terms( $product_id_exists, $tag, 'product_tag' );
						$imageID = saveFeaturedImage($sku,$product_id);
						if ($imageID){
							set_post_thumbnail( $product_id_exists, $imageID );	
						}
						
				}else{
					
				        $product_id = wp_insert_post( $product_data, true  );	
						update_post_meta( $product_id, '_sku', $sku );
						update_post_meta( $product_id, '_regular_price', $regular_price );
						update_post_meta( $product_id, '_stock', $stock );	
						update_post_meta( $product_id, '_price', $regular_price );	
						update_post_meta( $product_id, '_wholesale_price', $employee_price );
				
						if( $visibilty == 'N' ){
							update_post_meta( $product_id, '_visibility', 'hidden' );
						}else{
							update_post_meta( $product_id, '_visibility', 'visible' );
						}
						if( $stock == '0' ){
							update_post_meta( $product_id, '_stock_status', 'outofstock' );
							update_post_meta( $product_id, '_backorders', 'no' );

						}else{
							update_post_meta( $product_id, '_stock_status', 'instock' );
							update_post_meta( $product_id, '_backorders', 'yes' );
														
						}
				
						foreach( $product_meta_data as $p_k=>$p_v ){
							update_post_meta( $product_id, $p_k , $p_v );
						}
						wp_set_object_terms( $product_id, 'simple', 'product_type' );
						wp_set_object_terms( $product_id, $category, 'product_cat' );
						wp_set_object_terms( $product_id, $tag, 'product_tag' );
					
						$imageID = saveFeaturedImage($sku,$product_id);
						if ($imageID){
							set_post_thumbnail( $product_id, $imageID );	
						}
							
					
				}	
				
			//}
			echo '<br/>';
		}
		$row++;
	}
	fclose($file_handle);
	return $data;
}


function product_exist_new( $sku ) {
   global $wpdb;
   $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id,meta_value FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value= %s LIMIT 1", $sku ) );
   return $product_id;
}


function saveFeaturedImage($sku,$product_id) {
	
	$imageID = false;
	
	$attach_id = false;
	$upload_dir = wp_upload_dir();
	
	$image_path_dir = $_SERVER['DOCUMENT_ROOT'].'/images/';
	$image_path_url = site_url().'/images/';
	
	$filename_url = $image_path_url.$sku.'.jpg';
	$filename_dir = $image_path_dir.$sku.'.jpg';
	
	$imagename= $sku.'.jpg';
	
	if (file_exists($filename_dir)) {
		
		/* use curl to get image instead of $image_data = file_get_contents($image);*/
		
		/*$ch = curl_init();
		$timeout = 0;
		// curl set options
		curl_setopt ($ch, CURLOPT_URL, $filename_url);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt ($ch, CURLOPT_AUTOREFERER, true);
		
		// Getting binary data
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		
		//exec curl command
		$image_data = curl_exec($ch);
		
		*/
		/* !2.1.0 get the mime type incase there is no extension */
		//$mime_type =  curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

	
		//close the curl command
		//curl_close($ch);
		$image_data = file_get_contents($filename_url);
		//get the filename
		/* ! development add sanatize filename if name has spaces or %20 */
		$filename =  sanitize_file_name( basename(urldecode($filename_url)) );
		
		if (wp_mkdir_p($upload_dir['path'])) {
			$file = $upload_dir['path'] . '/' . $filename;
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}
		if(!file_exists($file)){
			//split it up
			list($directory, , $extension, $filename) = array_values(pathinfo($file));
			
			
			$wp_filetype = wp_check_filetype($imagename, null );
			
			if (file_put_contents($file, $image_data)) {
				
				$wp_filetype = wp_check_filetype($filename, null );
				
				/* ! 2.1.0 added mime type */
				/*if (!$wp_filetype['type'] && !empty($mime_type)) {
					$allowed_content_types = wp_get_mime_types();
					
					if (in_array($mime_type, $allowed_content_types)){
						$wp_filetype['type'] = $mime_type;
					}
				}*/
				
				$attachment = array(
					'post_mime_type' => $wp_filetype['type'],
					'post_title' => sanitize_file_name($filename),
					'post_content' => '',
					'post_status' => 'inherit'
				);

				$attach_id = wp_insert_attachment( $attachment, $file );
				require_once ABSPATH . 'wp-admin/includes/image.php';
				$attach_data = @wp_generate_attachment_metadata( $attach_id, $file );
				wp_update_attachment_metadata( $attach_id, $attach_data );	
				
			}
			 return $attach_id;	
		}else{
			return false;
		}
			
	}

}
	

function isValidUrl($url)
{
	// alternative way to check for a valid url
	// !development
	if  (filter_var($url, FILTER_VALIDATE_URL) === FALSE) return false; else return true;

}
		

//$csvFile = 'catlstk_1.csv';
//$csv = readCSV($csvFile);

//$dir = $_SERVER['DOCUMENT_ROOT'].'/import';


$dir =  $_SERVER['DOCUMENT_ROOT'].'/product_import'; // The directory containing the files. 
$ext = '.csv'; 

$files = glob($dir . '/*' . $ext);


foreach ($files as $csvFile) {
	//echo $csvFile;
	$csv = readCSVnew($csvFile);
}


?>