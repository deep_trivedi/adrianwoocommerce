<?php
include_once( $_SERVER['DOCUMENT_ROOT'].'/wp-load.php' );

$dir =  $_SERVER['DOCUMENT_ROOT'].'/usercsv'; // The directory containing the files. 
$ext = '.csv'; 
$files = glob( $dir . '/*' . $ext );
foreach ( $files as $csvFile ) {
	$csv = readCSVnew( $csvFile );
	echo $csv;
}



function readCSVnew( $csvFile ){
	global $wpdb;
	$row = 0;
	$file_handle = fopen( $csvFile, 'r' );
	ini_set('auto_detect_line_endings',TRUE);
	while ( !feof( $file_handle ) ) {
		$data= fgetcsv( $file_handle, 1024 );
		
		for($i = 0; $i < count($data); $i++){
			 $data[$i] = sc_acui_string_conversion($data[$i]);
		}
		
		$username=$data[0];
		$email=$data[1];
		$first_name=$data[2];
		$last_name=$data[3];
		if($data[4]=="Wholesaler" || $data[4]=="wholesaler")
		{
			$user_role='wholesale_customer';
		}
		elseif($data[4]=="Customer" || $data[4]=="customer" || $data[4]=="Regular customer" || $data[4]=="Regular Customer")
		{
			$user_role='regular_customer';
		}else{
			$user_role=$data[4];
		}
		if($data[5]=='')
		{
			$user_password=wp_generate_password();
		}
		else
		{
			$user_password=$data[5];
		}
		$nectar_id=$data[6];
		$sweetsource_id=$data[7];
		
		
		
		
		
		$core_fields = array(
	            'first_name' 		 => $first_name,
				'last_name' 		 => $last_name,
				//'user_email' 		 => $email,
	            //'role'		 		 => $user_role,	            
	            'nickname'	 		 => $username,
	            'display_name'		 => $first_name,
	            'description'		 => '',
				'password'			 => $user_password,
	            'nectar' 			 => $nectar_id,
				'sweetsource' 		 => $sweetsource_id,
				
        	);
						
		
		if($row == 0 ) {
			echo '<h3>Inserting and updating data</h3>
				<table>
					<tr><th>Row</th><th>Username</tr>';
			$row++;
		} else {
			if($username!='' && $email!='') {		
				if ( username_exists( $username ) ){	
										
					$user_object = get_user_by( "login", $username );
					$user_id = $user_object->ID;	
			
				} else {			
					$user_id = wp_create_user($username, $user_password, $email);		
					$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
					if ( $myaccount_page_id ) {
					  $myaccount_page_url = get_permalink( $myaccount_page_id );
					} else {
						$myaccount_page_url = get_admin_url();
					}
					$body_mail  = "Welcome,<br/>";
					$body_mail .= "Your data to login in this site is: <br/><br/>";
					$subject = get_bloginfo("name")." Login Details";
					
					$body_mail .= "<a href='" . $myaccount_page_url . "'>Login URL</a> <br/><br/>";
					$body_mail .= "Username : $username <br/>";
					$body_mail .= "Password : $user_password <br/><br/>";
					$body_mail .= "Nectar id : $nectar_id <br/><br/>";
					$body_mail .= "Sweetsource id : $sweetsource_id <br/><br/>";
					$body_mail .= "Thanks <br>";
					$body_mail .= get_bloginfo("name")." <br/><br/>";
					//add_filter( 'wp_mail_content_type', 'set_html_content_type' );
					wp_mail( $email, $subject, $body_mail );
					//remove_filter( 'wp_mail_content_type', 'set_html_content_type' );					
				}
				foreach($core_fields as $ukey => $uvalue){
					update_user_meta($user_id, $ukey, $uvalue);
					
				}
				wp_update_user( array( 'ID' => $user_id ) );
				$user = new WP_User($user_id);
				$old_role=$user->role;
				$user->remove_role($old_role);
				$user->add_role($user_role);
				//$query = $wpdb->replace( $wpdb->prefix.'groups_user_group', array('user_id' => $user_id, 'group_id' => $user_group ));
				
				echo "<tr><td>" . ($row - 1) . "</td>";
					echo "<td>$username $old_role $user_role</td>";
				echo "</tr>\n";
				flush();
			}
		}
		$row++;
	}
	echo '</table><br/>
	<p>Process finished you can go <a href="' . get_admin_url() . 'users.php">here to see results</a></p>';
	ini_set('auto_detect_line_endings',FALSE);
	fclose($file_handle);
}

function sc_acui_string_conversion($string){
	if(!preg_match('%(?:
    [\xC2-\xDF][\x80-\xBF]        # non-overlong 2-byte
    |\xE0[\xA0-\xBF][\x80-\xBF]               # excluding overlongs
    |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}      # straight 3-byte
    |\xED[\x80-\x9F][\x80-\xBF]               # excluding surrogates
    |\xF0[\x90-\xBF][\x80-\xBF]{2}    # planes 1-3
    |[\xF1-\xF3][\x80-\xBF]{3}                  # planes 4-15
    |\xF4[\x80-\x8F][\x80-\xBF]{2}    # plane 16
    )+%xs', $string)){
		return utf8_encode($string);
    }
	else
		return $string;
}
?>